
output "vm_ip_address" {
  value = "${openstack_compute_instance_v2.manager.network[0].fixed_ip_v4}"
}